SLURPEE THEME

This theme is a port of the Slurpee theme from phpWebSite (0.7.x and 0.8.x),
which can be found at http://phpwsthemes.sourceforge.net/

This theme features the ability to decide dynamically whether to draw the
left and right columns based upon whether you have blocks there.  If no
blocks are to be displayed on a particular page, the center column
expands to fill the space.

This theme is designed for Drupal 4.5.

Please direct any comments to tomdobes@purdue.edu
